<h1 align="center">Welcome to PolyMany MediaLibrary 👋</h1>
<p>
  <a href="https://twitter.com/_Epistol_">
    <img alt="Twitter: _Epistol_" src="https://img.shields.io/twitter/follow/_Epistol_.svg?style=social" target="_blank" />
  </a>
</p>

> An example project to display how the spatie mediaLibrary could handle a polymorphic many to many db.

> And it ryhmes.

## Install

```sh
git clone
npm install
```

## Description

Based on an empty Laravel package, this repository shows how 
the Spatie Media Library could handle a polymorphic many to many DB.

## Author

👤 **Epistol**

* Twitter: [@_Epistol_](https://twitter.com/_Epistol_)
* Github: [@Epistol](https://github.com/Epistol)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
